<?php

declare(strict_types=1);

namespace Skadmin\Role\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
