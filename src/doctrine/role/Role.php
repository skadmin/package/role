<?php

declare(strict_types=1);

namespace Skadmin\Role\Doctrine\Role;

use App\Model\Doctrine\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Nette\Security\Permission;
use SkadminUtils\DoctrineTraits\Entity;

use function boolval;

#[ORM\Entity]
#[ORM\Table(name: 'core_role')]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
#[ORM\HasLifecycleCallbacks]
class Role
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\IsLocked;
    use Entity\IsActive;

    #[ORM\Column(options: ['default' => false])]
    private bool $isMasterAdmin = false;

    /** @var User[]|ArrayCollection|Collection */
    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'roles')]
    private array|ArrayCollection|Collection $users;

    /** @var Permission[]|ArrayCollection|Collection */
    #[ORM\OneToMany(mappedBy: 'role', targetEntity: Privilege::class)]
    #[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
    private array|ArrayCollection|Collection $privileges;

    public function __construct()
    {
        $this->users      = new ArrayCollection();
        $this->privileges = new ArrayCollection();
    }

    // GETTER

    /**
     * Is master admin?
     */
    public function isMasterAdmin(): bool
    {
        return boolval($this->isMasterAdmin);
    }

    public function getUsers(): mixed
    {
        /** @var User[] $user */
        $user = $this->users;

        return $user;
    }

    public function getPrivileges(): mixed
    {
        /** @var Privilege[] $privileges */
        $privileges = $this->privileges;

        return $privileges;
    }

    // SETTER

    /**
     * update Role
     */
    public function updateRole(string $name, bool $isActive): void
    {
        $this->name = $name;
        $this->setIsActive($isActive);
    }
}
