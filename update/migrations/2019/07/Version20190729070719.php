<?php

declare(strict_types=1);

namespace DIS\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190729070719 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $resources = [
            [
                'name'        => 'user',
                'title'       => 'role-resource.user.title',
                'description' => 'role-resource.user.description',
            ],
            [
                'name'        => 'account',
                'title'       => 'role-resource.account.title',
                'description' => 'role-resource.account.description',
            ],
        ];

        foreach ($resources as $resource) {
            $this->addSql('INSERT INTO core_role_resource (name, title, description) VALUES (:name, :title, :description)', $resource);
        }
    }

    public function down(Schema $schema): void
    {
    }
}
