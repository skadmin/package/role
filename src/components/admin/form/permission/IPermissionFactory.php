<?php

declare(strict_types=1);

namespace Skadmin\Role\Components\Admin;

interface IPermissionFactory
{
    public function create(int $id): Permission;
}
