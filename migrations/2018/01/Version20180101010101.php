<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180101010101 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE core_role_privilege (id INT AUTO_INCREMENT NOT NULL, role_id INT DEFAULT NULL, resource_id INT DEFAULT NULL, p_read TINYINT(1) DEFAULT \'0\' NOT NULL, p_write TINYINT(1) DEFAULT \'0\' NOT NULL, p_delete TINYINT(1) DEFAULT \'0\' NOT NULL, additional_privilege LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_2D88A0D4D60322AC (role_id), INDEX IDX_2D88A0D489329D25 (resource_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE core_role (id INT AUTO_INCREMENT NOT NULL, is_master_admin TINYINT(1) DEFAULT \'0\' NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, is_locked TINYINT(1) DEFAULT \'0\' NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE core_role_resource (id INT AUTO_INCREMENT NOT NULL, additional_privilege LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', name VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE core_role_privilege ADD CONSTRAINT FK_2D88A0D4D60322AC FOREIGN KEY (role_id) REFERENCES core_role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE core_role_privilege ADD CONSTRAINT FK_2D88A0D489329D25 FOREIGN KEY (resource_id) REFERENCES core_role_resource (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE core_role_privilege DROP FOREIGN KEY FK_2D88A0D4D60322AC');
        $this->addSql('ALTER TABLE core_role_privilege DROP FOREIGN KEY FK_2D88A0D489329D25');
        $this->addSql('DROP TABLE core_role_privilege');
        $this->addSql('DROP TABLE core_role');
        $this->addSql('DROP TABLE core_role_resource');
    }
}
