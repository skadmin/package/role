##### replace
```text
from: "App\Model\Doctrine\Role\Role"
to: "Skadmin\Role\Doctrine\Role\Role"

from: "App\Model\Doctrine\Role\Privilege"
to: "Skadmin\Role\Doctrine\Role\Privilege"

from: "App\Model\Doctrine\Role\Source"
to: "Skadmin\Role\Doctrine\Role\Source"

from: "INSERT INTO role"
to: "INSERT INTO core_role"

from: "INSERT INTO resource"
to: "INSERT INTO core_role_resource"

from: "INSERT INTO privilege"
to: "INSERT INTO core_role_privilege"
```

##### move
```text
from: "update/migrations/Version20190805075323" 
to: "app/migrations/after"
```

##### modify text
```text
in "app/modules/adminModule/templates/_block/sidebar.latte" change link to role
```

##### delete
```text
file
- app/migrations/core/2019/07/Version20190727105313.php
- app/migrations/core/2019/07/Version20190729070719.php
- app/migrations/core/2020/05/Version20200512165023.php
- app/modules/adminModule/components/grid/GridRole.php
- app/modules/adminModule/presenters/RolePresenter.php

dir: 
- app/model/doctrine/role
- app/modules/adminModule/components/form/formPermissionsForRole
- app/modules/adminModule/templates/Role
```

##### SQL
```sql
# BEFORE
INSERT INTO _nettrine_migrations_after (version, executed_at, execution_time) VALUES ('Migrations\\Version20190727105313', now(), NULL);
INSERT INTO _nettrine_migrations_after (version, executed_at, execution_time) VALUES ('Migrations\\Version20190729070719', now(), NULL);
ALTER TABLE mail_template CHANGE recipients recipients LONGTEXT DEFAULT '' NOT NULL COMMENT '(DC2Type:array)', CHANGE preheader preheader VARCHAR(255) DEFAULT '' NOT NULL;

# AFTER
ALTER TABLE privilege DROP FOREIGN KEY FK_87209A8789329D25;
ALTER TABLE privilege DROP FOREIGN KEY FK_87209A87D60322AC;
ALTER TABLE user_role_rel DROP FOREIGN KEY FK_A055B7CFD60322AC;

SET foreign_key_checks = 0;
TRUNCATE TABLE core_role;
TRUNCATE TABLE core_role_privilege;
TRUNCATE TABLE core_role_resource;

INSERT INTO core_role_resource (name, title, description, additional_privilege) SELECT name, title, description, additional_privilege FROM resource;
INSERT INTO core_role (is_master_admin, webalize, name, is_locked, is_active) SELECT is_master_admin, webalize, name, is_locked, is_active FROM role;
INSERT INTO core_role_privilege (role_id, resource_id, p_read, p_write, p_delete, additional_privilege) SELECT role_id, resource_id, p_read, p_write, p_delete, additional_privilege FROM privilege;
SET foreign_key_checks = 1;

ALTER TABLE user_role_rel ADD CONSTRAINT FK_A055B7CFD60322AC FOREIGN KEY (role_id) REFERENCES core_role (id) ON DELETE CASCADE;

DROP TABLE privilege;
DROP TABLE resource;
DROP TABLE role;

DELETE FROM _nettrine_migrations WHERE version LIKE '%20190727105313';
DELETE FROM _nettrine_migrations WHERE version LIKE '%20190729070719';
DELETE FROM _nettrine_migrations WHERE version LIKE '%20190729102025';
DELETE FROM _nettrine_migrations WHERE version LIKE '%20200512165023';
DELETE FROM _nettrine_migrations WHERE version LIKE '%20190729104959';
```

##### check resource
```text
check duplicite resource "role"
```
