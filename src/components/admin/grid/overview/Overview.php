<?php

declare(strict_types=1);

namespace Skadmin\Role\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Container;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\Role\BaseControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Role\Doctrine\Role\Role;
use Skadmin\Role\Doctrine\Role\RoleFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;

use function boolval;
use function count;
use function implode;
use function intval;

class Overview extends GridControl
{
    use APackageControl;

    private RoleFacade $facade;

    public function __construct(RoleFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'role.overview.title';
    }

    protected function createComponentGrid(): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // DATA
        $translator = $this->translator;
        $dialYesNo  = Arrays::map(Constant::DIAL_YES_NO, static function ($text) use ($translator): string {
            return $translator->translate($text);
        });

        // COLUMNS
        $grid->addColumnText('name', 'grid.roles.name');
        $grid->addColumnText('isActive', 'grid.roles.is-active')
            ->setAlign('center')
            ->setReplacement($dialYesNo);
        $grid->addColumnText('permission', 'grid.roles.permission')
            ->setRenderer(static function (Role $role) use ($translator): Html {
                if ($role->isMasterAdmin()) {
                    return Html::el('span', ['class' => 'badge badge-primary mb-1 mt-1'])->setText($translator->translate('resource.all'));
                }

                $htmlPrivileges = new Html();
                foreach ($role->getPrivileges() as $privilege) {
                    $privileges = $privilege->getAllowPrivilege(true);

                    if (count($privileges) <= 0) {
                        continue;
                    }

                    $htmlPrivilege = Html::el('span', ['class' => 'badge badge-primary mr-2 mb-1 mt-1'])
                        ->addText($translator->translate($privilege->getResource()->getTitle()));

                    $countAdditionalPrivilege = count($privilege->getResource()->getAdditionalPrivilege());
                    if (count($privileges) !== $countAdditionalPrivilege + count(Privilege::PRIVILEGES)) {
                        $_privileges = Arrays::map($privileges, static function ($privilege) use ($translator): string {
                            return $translator->translate($privilege);
                        });
                        $htmlPrivilege->addText(': ')
                            ->addText(implode(', ', $_privileges));
                    }

                    $htmlPrivileges->addHtml($htmlPrivilege);
                }

                return $htmlPrivileges;
            });

        // FILTER
        $grid->addFilterText('name', 'grid.users.name');
        $grid->addFilterSelect('isActive', 'grid.users.is-active', Constant::PROMTP_ARR + Constant::DIAL_YES_NO)
            ->setTranslateOptions();

        // IF ROLE ALLOWED WRITE
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            // INLINE ADD
            $grid->addInlineAdd()
                ->setPositionTop()
                ->setText($this->translator->translate('grid.roles.action.inline-add'))
                ->onControlAdd[] = [$this, 'onInlineAdd'];
            $grid->getInlineAddPure()
                ->onSubmit[]     = [$this, 'onInlineAddSubmit'];

            // INLINE EDIT
            $grid->addInlineEdit()
                ->onControlAdd[]                        = [$this, 'onInlineEdit'];
            $grid->getInlineEditPure()->onSetDefaults[] = [$this, 'onInlineEditDefaults'];
            $grid->getInlineEditPure()->onSubmit[]      = [$this, 'onInlineEditSubmit'];

            // ACTION
            $grid->addAction('permission', 'grid.roles.action.permission', 'Component:default', ['id' => 'id'])
                ->addParameters([
                    'package' => new BaseControl(),
                    'render'  => 'permission',
                ])->setIcon('pencil-ruler')
                ->setClass('btn btn-xs btn-primary');
        }

        // ALLOW
        $grid->allowRowsAction('permission', static fn (Role $role): bool => ! $role->isMasterAdmin());
        $grid->allowRowsInlineEdit(static fn (Role $role): bool => ! $role->isMasterAdmin());

        return $grid;
    }

    public function onInlineAdd(Container $container): void
    {
        $container->addText('name', 'grid.roles.name')
            ->setRequired('grid.roles.name.req');
        $container->addSelect('isActive', 'grid.roles.is-active', Constant::DIAL_YES_NO)
            ->setDefaultValue(Constant::YES);
    }

    /**
     * @param ArrayHash<mixed> $values
     */
    public function onInlineAddSubmit(ArrayHash $values): void
    {
        $this->facade->createRole($values->name, boolval($values->isActive));

        $message = new SimpleTranslation('grid.roles.action.flash.inline-add.success "%s"', [$values->name]);
        $this->getPresenter()->flashMessage($message, Flash::SUCCESS);
    }

    public function onInlineEdit(Container $container): void
    {
        $container->addText('name', 'grid.roles.name')
            ->setRequired('grid.roles.name.req');
        $container->addSelect('isActive', 'grid.roles.is-active', Constant::DIAL_YES_NO);
    }

    public function onInlineEditDefaults(Container $container, Role $role): void
    {
        $container->setDefaults([
            'name'     => $role->getName(),
            'isActive' => intval($role->isActive()),
        ]);
    }

    /**
     * @param int|string       $id
     * @param ArrayHash<mixed> $values
     */
    public function onInlineEditSubmit($id, ArrayHash $values): void
    {
        $this->facade->updateRole(intval($id), $values->name, boolval($values->isActive));
        $message = new SimpleTranslation('grid.roles.action.flash.inline-edit.success "%s"', [$values->name]);
        $this->getPresenter()->flashMessage($message, Flash::SUCCESS);
    }
}
