<?php

declare(strict_types=1);

namespace Skadmin\Role\Doctrine\Role;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

use function array_keys;
use function boolval;
use function in_array;
use function is_array;
use function is_bool;
use function sprintf;

#[ORM\Entity]
#[ORM\Table(name: 'core_role_privilege')]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class Privilege
{
    public const READ       = 'read';
    public const WRITE      = 'write';
    public const DELETE     = 'delete';
    public const PRIVILEGES = [
        self::READ,
        self::WRITE,
        self::DELETE,
    ];
    use Entity\Id;

    #[ORM\Column(name: 'p_read', options: ['default' => false])]
    private bool $read = false;

    #[ORM\Column(name: 'p_write', options: ['default' => false])]
    private bool $write = false;

    #[ORM\Column(name: 'p_delete', options: ['default' => false])]
    private bool $delete = false;

    /** @var bool[]|null */
    #[ORM\Column(type: Types::ARRAY, nullable: true)]
    private ?array $additionalPrivilege = null;

    #[ORM\ManyToOne(targetEntity: Role::class, inversedBy: 'privileges')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Role $role;

    #[ORM\ManyToOne(targetEntity: Source::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Source $resource;

    /**
     * @return string[]
     */
    public function getAllowPrivilege(bool $usePrefix = false): array
    {
        $allow = [];

        if ($this->isRead()) {
            $allow[] = self::READ;
        }

        if ($this->isWrite()) {
            $allow[] = self::WRITE;
        }

        if ($this->isDelete()) {
            $allow[] = self::DELETE;
        }

        $addPrivileges = $this->getAdditionalPrivilege();

        if (is_array($addPrivileges)) {
            foreach (array_keys($addPrivileges) as $addPrivilege) {
                if ($usePrefix) {
                    $allow[] = sprintf('privilege.%s', $addPrivilege);
                } else {
                    $allow[] = $addPrivilege;
                }
            }
        }

        return $allow;
    }

    /**
     * @param bool|int $read
     * @param bool|int $write
     * @param bool|int $delete
     * @param string[] $additionalPrivilege
     */
    public function updatePrivilege($read, $write, $delete, array $additionalPrivilege): void
    {
        $this->read   = boolval($read);
        $this->write  = boolval($write);
        $this->delete = boolval($delete);

        $this->additionalPrivilege = [];
        foreach ($additionalPrivilege as $addPrivilege) {
            $this->additionalPrivilege[$addPrivilege] = true;
        }
    }

    public function isRead(): bool
    {
        return boolval($this->read);
    }

    public function isWrite(): bool
    {
        return boolval($this->write);
    }

    public function isDelete(): bool
    {
        return boolval($this->delete);
    }

    public function isPrivilege(string $privilege): bool
    {
        if (in_array($privilege, self::PRIVILEGES, true)) {
            return boolval($this->{$privilege});
        }

        $result = $this->getAdditionalPrivilege($privilege);

        return is_bool($result) ? $result : false;
    }

    public function getRole(): Role
    {
        return $this->role;
    }

    public function getResource(): Source
    {
        return $this->resource;
    }

    public function createPrivilege(Role $role, Source $resource): void
    {
        $this->role     = $role;
        $this->resource = $resource;
    }

    /**
     * @return bool[]|bool
     */
    public function getAdditionalPrivilege(?string $privilege = null): array|bool
    {
        if (! is_array($this->additionalPrivilege)) {
            $this->additionalPrivilege = [];
        }

        if ($privilege === null) {
            return $this->additionalPrivilege;
        }

        return $this->additionalPrivilege[$privilege] ?? false;
    }
}
