<?php

declare(strict_types=1);

namespace Skadmin\Role\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Role\BaseControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Role\Doctrine\Role\Role;
use Skadmin\Role\Doctrine\Role\RoleFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;

use function array_combine;
use function array_search;
use function in_array;
use function sprintf;

class Permission extends FormWithUserControl
{
    use APackageControl;

    private RoleFacade $facadeRole;
    private Role       $role;

    public function __construct(int $id, RoleFacade $facadeRole, Translator $translator, LoggedUser $user)
    {
        parent::__construct($translator, $user);

        $this->facadeRole = $facadeRole;
        $this->role       = $this->facadeRole->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation
    {
        return new SimpleTranslation('role.permission.title %s', [$this->role->getName()]);
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/permission.latte');
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $dataPrivilege = array_combine(Privilege::PRIVILEGES, Privilege::PRIVILEGES);

        // INPUT
        $formPrivilege = $form->addContainer('privilege');
        foreach ($this->facadeRole->getAllResources() as $resource) {
            $addPrivileges = [];
            foreach ($resource->getAdditionalPrivilege() as $addPrivilege) {
                $addPrivileges[$addPrivilege] = sprintf('privilege.%s', $addPrivilege);
            }

            $formPrivilege->addCheckboxList((string) $resource->getId(), $resource->getTitle(), $dataPrivilege + $addPrivileges)
                ->setHtmlAttribute('data-description', $resource->getDescription());
        }

        // BUTTON
        $form->addSubmit('send', 'form.permissions-for-role.send');
        $form->addSubmit('sendBack', 'form.permissions-for-role.send-back');
        $form->addSubmit('back', 'form.permissions-for-role.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        $defaults = [
            'privilege' => [],
        ];

        foreach ($this->role->getPrivileges() as $privilege) {
            $defaults['privilege'][$privilege->getResource()->getId()] = $privilege->getAllowPrivilege();
        }

        return $defaults;
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $this->onSuccess($form, $values);

        foreach ($values->privilege as $resource => $privilege) {
            $read   = in_array(Privilege::READ, $privilege, true);
            $write  = in_array(Privilege::WRITE, $privilege, true);
            $delete = in_array(Privilege::DELETE, $privilege, true);

            // remove default privilege
            foreach (Privilege::PRIVILEGES as $val) {
                $key = array_search($val, $privilege, true);
                if ($key === false) {
                    continue;
                }

                unset($privilege[$key]);
            }

            $this->facadeRole->createPrivilege(
                $this->role,
                $resource,
                $read,
                $write,
                $delete,
                $privilege
            );
        }

        $this->onFlashmessage('form.permissions-for-role.flash.success', Flash::SUCCESS);

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->redrawControl('snipForm');
    }
}
