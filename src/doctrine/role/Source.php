<?php

declare(strict_types=1);

namespace Skadmin\Role\Doctrine\Role;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Strings;
use SkadminUtils\DoctrineTraits\Entity;

use function is_array;
use function sprintf;

#[ORM\Entity]
#[ORM\Table(name: 'core_role_resource')]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class Source
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Title;
    use Entity\Description;

    /** @var string[]|null */
    #[ORM\Column(type: Types::ARRAY, nullable: true)]
    private ?array $additionalPrivilege = null;

    public function createResource(string $name): void
    {
        $this->name        = Strings::webalize($name);
        $this->title       = sprintf('role-resource.%s.title', $this->name);
        $this->description = sprintf('role-resource.%s.description', $this->name);
    }

    /**
     * @return array|string[]
     */
    public function getAdditionalPrivilege(): array
    {
        if (! is_array($this->additionalPrivilege)) {
            $this->additionalPrivilege = [];
        }

        return $this->additionalPrivilege;
    }
}
